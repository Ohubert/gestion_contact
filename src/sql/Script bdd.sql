CREATE TABLE Eleve(
    Eleve_Id INT(20) AUTO_INCREMENT,
    classe VARCHAR(25),
    specialité VARCHAR(25),
    PRIMARY KEY (Eleve_Id)
);

CREATE TABLE Organisation(
    Organisation_Id INT(20) AUTO_INCREMENT,
    id VARCHAR(25),
    libelle VARCHAR(25),
    adresse VARCHAR(25),
    professionnel INT(20),
    PRIMARY KEY (Organisation_Id),
    UNIQUE (professionnel)
);

CREATE TABLE Professeur(
    Professeur_Id INT(20) AUTO_INCREMENT,
    PRIMARY KEY (Professeur_Id)
);

CREATE TABLE Visite(
    Visite_Id INT(20) AUTO_INCREMENT,
    id VARCHAR(25),
    date VARCHAR(25),
    stage INT(20),
    PRIMARY KEY (Visite_Id),
    UNIQUE (stage)
);

CREATE TABLE Personne(
    Personne_Id INT(20) AUTO_INCREMENT,
    id VARCHAR(25),
    nom VARCHAR(25),
    prenom VARCHAR(25),
    mail VARCHAR(25),
    telephone VARCHAR(25),
    PRIMARY KEY (Personne_Id)
);

CREATE TABLE Stage(
    Stage_Id INT(20) AUTO_INCREMENT,
    id VARCHAR(25),
    intitule VARCHAR(25),
    date_debut VARCHAR(25),
    date_fin VARCHAR(25),
    organisation INT(20),
    PRIMARY KEY (Stage_Id)
);

CREATE TABLE Promo(
    Promo_Id INT(20) AUTO_INCREMENT,
    Promotion VARCHAR(25),
    PRIMARY KEY (Promo_Id)
);

CREATE TABLE Professionnel(
    Professionnel_Id INT(20) AUTO_INCREMENT,
    PRIMARY KEY (Professionnel_Id)
);

CREATE TABLE Annee(
    Annee_Id INT(20) AUTO_INCREMENT,
    PRIMARY KEY (Annee_Id)
);



ALTER TABLE Organisation ADD FOREIGN KEY (professionnel) REFERENCES Professionnel(Professionnel_Id);
ALTER TABLE Visite ADD FOREIGN KEY (stage) REFERENCES Stage(Stage_Id);
ALTER TABLE Stage ADD FOREIGN KEY (organisation) REFERENCES Organisation(Organisation_Id);